import 'package:flutter/material.dart';
import 'package:insta_clone/repository/data_repository.dart';
import 'package:insta_clone/ui/widget/feed_list_item.dart';

class FeedList extends StatelessWidget {
  const FeedList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final feed = DataRepository().getFeed();

    return ListView.builder(
        physics: ClampingScrollPhysics(),
        padding: EdgeInsets.only(top: 10),
        shrinkWrap: true,
        itemCount: feed.length,
        itemBuilder: (context, index) {
          return FeedListItem(post: feed[index]);
        });
  }
}
