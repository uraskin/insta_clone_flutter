import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:insta_clone/utils/app_icons_icons.dart';

class BottomBarMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar (
        child: new Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        new IconButton(
          icon: Icon(
            AppIcons.home,
          ),
          onPressed: () {},
        ),
        new IconButton(
          icon: Icon(
            AppIcons.search, color: Colors.black
          ),
          onPressed: null,
        ),
        new IconButton(
          icon: Icon(
            AppIcons.upload,color: Colors.black
          ),
          onPressed: null,
        ),
        new IconButton(
          icon: Icon(
            AppIcons.like, color: Colors.black
          ),
          onPressed: null,
        ),
        new IconButton(
          icon: Icon(
            AppIcons.profile, color: Colors.black
          ),
          onPressed: null,
        ),
      ],
    ));
  }
}
