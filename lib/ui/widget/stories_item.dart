import 'package:flutter/material.dart';

class StoriesItem extends StatelessWidget {
  final String label;
  final String avatar;

  StoriesItem({Key key, @required this.label, @required this.avatar})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(2.5),
                height: 60,
                width: 60,
                child: CircleAvatar(
                  backgroundImage: ExactAssetImage(avatar),
                ),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.pinkAccent, width: 2),
                  shape: BoxShape.circle,
                )),
            SizedBox(height: 4),
            Text(label, style: TextStyle(fontSize: 12))
          ],
        ));
  }
}
