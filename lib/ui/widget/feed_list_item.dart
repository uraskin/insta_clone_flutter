import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:insta_clone/model/post.dart';
import 'package:insta_clone/ui/screen/post_comments.dart';
import 'package:insta_clone/utils/app_icons_icons.dart';

class FeedListItem extends StatelessWidget {
  final Post post;

  FeedListItem({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                  backgroundImage: ExactAssetImage(post.avatarImage)),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(post.author,
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 15)),
                  Text(post.place)
                ],
              ),
            )),
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Icon(
                AppIcons.more,
                size: 16,
              ),
            )
          ],
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Image(
            image: ExactAssetImage(post.imagePhoto),
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    Icon(AppIcons.like),
                    SizedBox(width: 15),
                    GestureDetector(
                      onTap: () => Navigator.of(context)
                              .push(MaterialPageRoute(builder: (_) {
                            return PostComments();
                          })),
                      child: Icon(AppIcons.chat),
                    ),
                    SizedBox(width: 15),
                    Icon(AppIcons.message),
                  ],
                ),
              ),
              Row(
                children: <Widget>[Icon(AppIcons.bookmark)],
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10.0, top: 10),
          child: Text('Нравится ${post.likeCount} и еще 25',
              style: TextStyle(fontWeight: FontWeight.w700)),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 10.0, top: 5),
          child: Row(
            children: <Widget>[
              Text(post.author, style: TextStyle(fontWeight: FontWeight.w700)),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Text(post.description),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text('Посмотреть все комментарии (${post.commentsCount})',
              style: TextStyle(fontSize: 15, color: Colors.black45)),
        ),
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 5),
              child: CircleAvatar(
                  radius: 15,
                  backgroundImage: ExactAssetImage(post.avatarImage)),
            ),
            Text('Добавьте комментарий...',
                style: TextStyle(fontSize: 15, color: Colors.black45))
          ],
        ),
        SizedBox(
          height: 15,
        )
      ],
    );
  }
}
