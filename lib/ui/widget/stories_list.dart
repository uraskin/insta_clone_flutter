import 'package:flutter/material.dart';
import 'package:insta_clone/repository/data_repository.dart';
import 'package:insta_clone/ui/widget/stories_item.dart';
import 'package:insta_clone/ui/widget/stories_self_item.dart';

class StoriesList extends StatelessWidget {
  final stories = DataRepository().getStoiries();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.only(top: 10, left: 5),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: stories.length,
        itemBuilder: (context, index) {
          return index == 0
              ? StoriesSelfItem()
              : StoriesItem(
                  avatar: stories[index].avatarImage,
                  label: stories[index].author);
        });
  }
}
