import 'package:flutter/material.dart';

class StoriesSelfItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
      Container(
          height: 60,
          width: 60,
          child: Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: Colors.white),
              child:
                  Icon(Icons.add_circle, color: Colors.blueAccent, size: 17)),
          alignment: Alignment.bottomRight,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: ExactAssetImage('assets/img/avatars/wesley.jpg')))),
      SizedBox(height: 4),
      Text('Ваша история', style: TextStyle(fontSize: 12))
    ]);
  }
}
