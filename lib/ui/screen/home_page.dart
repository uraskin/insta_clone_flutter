import 'package:flutter/material.dart';
import 'package:insta_clone/ui/widget/bottom_bar_menu.dart';
import 'package:insta_clone/ui/widget/feed_list.dart';
import 'package:insta_clone/ui/widget/stories_list.dart';
import 'package:insta_clone/utils/app_icons_icons.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Color(0xfff8faf8),
            centerTitle: true,
            title: SizedBox(
                height: 35.0, child: Image.asset("assets/img/insta_logo.png")),
            leading: Icon(AppIcons.camera),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Icon(AppIcons.igtv),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15.0),
                child: Icon(
                  AppIcons.message,
                  size: 20,
                ),
              )
            ]),
        bottomNavigationBar: BottomBarMenu(),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 100,
                child: StoriesList(),
              ),
              Container(
                height: 0.5,
                color: Colors.black12,
              ),
              FeedList()
            ],
          ),
        ));
  }
}
