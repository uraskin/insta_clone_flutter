import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PostComments extends StatelessWidget {
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).requestFocus(_focusNode);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xfff8faf8),
        title: Text('Комментарии'),
      ),
      body: TextField(
        focusNode: _focusNode,
      ),
    );
  }
}
