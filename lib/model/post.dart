class Post {
  String author;
  String place;
  int likeCount;
  int commentsCount;
  String description;
  String imagePhoto;
  String avatarImage;

  Post(
      {this.author,
      this.commentsCount,
      this.description,
      this.likeCount,
      this.place,
      this.imagePhoto,
      this.avatarImage});
}
