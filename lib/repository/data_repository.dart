
import 'package:insta_clone/model/post.dart';
import 'package:insta_clone/model/stories.dart';

class DataRepository {
  List<Stories> getStoiries() {
    List<Stories> stroies = List();
    stroies.add(Stories(author: 'Anna', avatarImage: 'assets/img/avatars/anna.jpg'));
    stroies.add(Stories(author: 'Alex', avatarImage: 'assets/img/avatars/alex.jpg'));
    stroies.add(Stories(author: 'Andy', avatarImage: 'assets/img/avatars/andy.jpg'));
    stroies.add(Stories(author: 'Arshad', avatarImage: 'assets/img/avatars/arshad.jpg'));
    stroies.add(Stories(author: 'Benjamin', avatarImage: 'assets/img/avatars/benjamin.jpg'));
    stroies.add(Stories(author: 'Carla', avatarImage: 'assets/img/avatars/carlos.jpg'));
    stroies.add(Stories(author: 'Lachlan', avatarImage: 'assets/img/avatars/lachlan.jpg'));
    stroies.add(Stories(author: 'Luis', avatarImage: 'assets/img/avatars/luis.jpg'));
    stroies.add(Stories(author: 'Nicolas', avatarImage: 'assets/img/avatars/nicolas.jpg'));

    return stroies;
  }

  List<Post> getFeed() {
    List<Post> feed = List();
    feed.add(Post(author: 'Alex', avatarImage: 'assets/img/avatars/alex.jpg', place: 'Локация', commentsCount: 2, likeCount: 150, description: 'Дамба на реке', imagePhoto: 'assets/img/feed/photo1.jpg'));
    feed.add(Post(author: 'Andy', avatarImage: 'assets/img/avatars/andy.jpg', place: 'Локация', commentsCount: 1, likeCount: 10, description: 'Замок на вершине горы', imagePhoto: 'assets/img/feed/photo2.jpg'));
    feed.add(Post(author: 'Anna', avatarImage: 'assets/img/avatars/anna.jpg', place: 'Где-то у моря', commentsCount: 6, likeCount: 194, description: 'Божественный заказ', imagePhoto: 'assets/img/feed/photo3.jpg'));
    feed.add(Post(author: 'Arshad', avatarImage: 'assets/img/avatars/arshad.jpg', place: 'Локация', commentsCount: 5, likeCount: 28, description: 'Озеро', imagePhoto: 'assets/img/feed/photo4.jpg'));
    feed.add(Post(author: 'Benjamin', avatarImage: 'assets/img/avatars/benjamin.jpg', place: 'Локация', commentsCount: 12, likeCount: 28, description: 'Новый мерс', imagePhoto: 'assets/img/feed/photo5.jpg'));
    feed.add(Post(author: 'Carla', avatarImage: 'assets/img/avatars/carlos.jpg', place: 'В офисе Google', commentsCount: 12, likeCount: 28, description: 'Рабочее место', imagePhoto: 'assets/img/feed/photo6.jpg'));
    feed.add(Post(author: 'Lachlan', avatarImage: 'assets/img/avatars/lachlan.jpg', place: 'Локация', commentsCount: 7, likeCount: 58, description: '', imagePhoto: 'assets/img/feed/photo7.jpg'));
    feed.add(Post(author: 'Luis', avatarImage: 'assets/img/avatars/luis.jpg', place: 'Локация', commentsCount: 7, likeCount: 158, description: '', imagePhoto: 'assets/img/feed/photo8.jpg'));
    feed.add(Post(author: 'Parker', avatarImage: 'assets/img/avatars/parker.jpg', place: 'Локация', commentsCount: 7, likeCount: 158, description: '', imagePhoto: 'assets/img/feed/photo9.jpg'));
    feed.add(Post(author: 'Wesley', avatarImage: 'assets/img/avatars/wesley.jpg', place: 'Локация', commentsCount: 7, likeCount: 158, description: '', imagePhoto: 'assets/img/feed/photo10.jpg'));
    return feed;
  }
}